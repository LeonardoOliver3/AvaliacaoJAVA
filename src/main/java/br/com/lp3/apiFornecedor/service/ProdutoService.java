package br.com.lp3.apiFornecedor.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.lp3.apiFornecedor.dto.FornecedorDTO;
import br.com.lp3.apiFornecedor.dto.ProdutoDTO;
import br.com.lp3.apiFornecedor.model.Fornecedor;
import br.com.lp3.apiFornecedor.model.Produto;
import br.com.lp3.apiFornecedor.repository.FornecedorRepository;
import br.com.lp3.apiFornecedor.repository.ProdutoRepository;

public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;

	public ProdutoDTO findById(Long id) {
		Optional<Produto> oProduto = produtoRepository.findById(id);
		if (oProduto != null && oProduto.isPresent()) {
			Produto produto = oProduto.get();
			ProdutoDTO produtoDTO = new ProdutoDTO(produto.getId(), produto.getNome(), produto.getDescricao(), produto.getValor(), produto.getFornecedor());
			return produtoDTO;
		} else {
			return null;
		}
		
	}
	
	public ProdutoDTO saveProduto(ProdutoDTO produtoDTO) {
		Produto produto = new Produto(produtoDTO.getId(), produtoDTO.getNome(), produtoDTO.getDescricao(), produtoDTO.getValor(), produtoDTO.getFornecedor());
		produto= produtoRepository.save(produto);
		produtoDTO.setId(produto.getId());
		return produtoDTO;
	}
	
	public ProdutoDTO deletarProduto(ProdutoDTO produtoDTO) {
		Produto produto = new Produto(produtoDTO.getId(), produtoDTO.getNome(), produtoDTO.getDescricao(), produtoDTO.getValor(), produtoDTO.getFornecedor());
		produto= produtoRepository.delete(produtoDTO);
		produtoDTO.setId(produto.getId());
		return produtoDTO;
	}
	
}
