package br.com.lp3.apiFornecedor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.apiFornecedor.dto.ClienteDTO;
import br.com.lp3.apiFornecedor.model.Cliente;
import br.com.lp3.apiFornecedor.repository.ClienteRepository;



@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository _clienteRepository;
	
	public ClienteDTO getClienteById(int id) {
		
		Optional<Cliente> oCliente = _clienteRepository.findById((long) id);
		
		if(oCliente != null && oCliente.isPresent()) {
			Cliente cliente = oCliente.get();
			ClienteDTO clienteDTO = new ClienteDTO(); 
			
			
			clienteDTO.setId(cliente.getId());
			clienteDTO.getNome();
			clienteDTO.getCpf();
			
			return clienteDTO;
			
		} else {
			
			return null;
		}
	}
	
	public List<ClienteDTO> getClientes() {
		Iterable<Cliente> iClientes = _clienteRepository.findAll();
		List<ClienteDTO> ps = new ArrayList<ClienteDTO>();
		
		for(Cliente cliente : iClientes) {
			
			ClienteDTO clienteDTO = new ClienteDTO();
			clienteDTO.getId();
			clienteDTO.getNome();
			clienteDTO.getCpf();
	
			ps.add(clienteDTO);
		}
		
		return ps;
	}
	
	public ClienteDTO addCliente(ClienteDTO clienteDTO) {
			
		Cliente cliente = new Cliente();
		
		clienteDTO.getNome();
		clienteDTO.getCpf();
		
		cliente = _clienteRepository.save(cliente);
		clienteDTO.setId(cliente.getId());
		return clienteDTO;
	
	}
	
	public ClienteDTO editCliente(ClienteDTO clienteDTO) {
		
		Cliente cliente = new Cliente();
		
		clienteDTO.getNome();
		clienteDTO.getCpf();
		
		cliente = _clienteRepository.save(cliente);
		
		return clienteDTO;
	
	}
	
	public ClienteDTO deleteCliente(ClienteDTO clienteDTO) { 
		
		Cliente cliente = new Cliente();
		cliente.getId();
		cliente.getNome();
		cliente.getCpf();
		
		_clienteRepository.delete(cliente);
		return clienteDTO;
	}

	public ClienteDTO findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
