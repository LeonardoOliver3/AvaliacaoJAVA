package br.com.lp3.apiFornecedor.dto;

import br.com.lp3.apiFornecedor.model.Fornecedor;

public class ProdutoDTO {
	
	private long id;
	private String nome;
	private String descricao;
	private double valor;
	private Fornecedor fornecedor;
	private int estoque;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public int getEstoque() {
		return estoque;
	}
	public void setEstoque(int estoque) {
		this.estoque = estoque;
	}
	public ProdutoDTO(long id, String nome, String descricao, double valor, Fornecedor fornecedor) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
		this.fornecedor = fornecedor;
	}
	public ProdutoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	

}
