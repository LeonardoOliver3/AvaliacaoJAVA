package br.com.lp3.apiFornecedor.dto;

import br.com.lp3.apiFornecedor.model.Cliente;
import br.com.lp3.apiFornecedor.model.Produto;

public class PedidoDTO {
	
	private long id;
	private String descricao;
	private Produto produto;
	private Cliente cliente;
	private int quantidade;
	private double valorTotal;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}
	public PedidoDTO(long id, String descricao, Produto produto, Cliente cliente, int quantidade, double valorTotal) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.produto = produto;
		this.cliente = cliente;
		this.quantidade = quantidade;
		this.valorTotal = valorTotal;
	}
	public PedidoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
