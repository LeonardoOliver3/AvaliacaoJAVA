package br.com.lp3.apiFornecedor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.apiFornecedor.dto.PedidoDTO;
import br.com.lp3.apiFornecedor.service.PedidoService;


@RestController
@RequestMapping(value = "/pedido")
public class PedidoController {

	
	@Autowired
	private PedidoService pedidoService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<PedidoDTO> findClienteById(@PathVariable Long id) {
		PedidoDTO pedidoDTO = pedidoService.findById(id);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<PedidoDTO> savePedido(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = pedidoService.addPedido(pedidoDTO);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.PUT)
	public ResponseEntity<PedidoDTO> editPedido(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = pedidoService.editPedido(pedidoDTO);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.DELETE)
	public ResponseEntity<PedidoDTO> deletePedido(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = pedidoService.deletePedido(pedidoDTO);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
