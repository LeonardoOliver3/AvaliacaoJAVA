package br.com.lp3.apiFornecedor.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.lp3.apiFornecedor.model.Cliente;



@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>{

}


