package br.com.lp3.apiFornecedor.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.lp3.apiFornecedor.dto.ProdutoDTO;
import br.com.lp3.apiFornecedor.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long>{

	Produto delete(ProdutoDTO produtoDTO);

}
