package br.com.lp3.apiFornecedor.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.lp3.apiFornecedor.model.Pedido;



@Repository
public interface PedidoRepository extends CrudRepository<Pedido, Long>{

}
